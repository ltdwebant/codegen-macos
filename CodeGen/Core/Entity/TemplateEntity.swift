//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

class TemplateEntity: Codable {

    var name: String
    var url: URL

    init(name: String, url: URL){
        self.name = name
        self.url = url
    }
}
