//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

protocol SettingsGateway {

    var templates: [TemplateEntity] { set get }

    var placeholder: String { get set }

    func addTemplate(_ template: TemplateEntity)

    func removeTemplate(_ index: Int)
}
