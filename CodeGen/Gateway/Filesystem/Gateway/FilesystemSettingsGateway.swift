//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

class FilesystemSettingsGateway: SettingsGateway, Codable {

    var templates: [TemplateEntity] = []
    var placeholder: String = "~!(.+?)!~" {
        didSet {
            save()
        }
    }

    func addTemplate(_ template: TemplateEntity) {
        templates.append(template)
        save()
    }

    func removeTemplate(_ index: Int) {
        templates.remove(at: index)
        save()
    }

    private init() {
    }

    private init(_ templates: [TemplateEntity], _ placeholder: String) {
        self.templates = templates
        self.placeholder = placeholder
    }

    func save() {
        let data = try! JSONEncoder().encode(self)
        let path = FilesystemSettingsGateway.getAppPath()
        try! FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true)
        FileManager.default.createFile(atPath: FilesystemSettingsGateway.getSettingFilePath(), contents: data)
    }

    private class func getSettingFilePath() -> String {
        return "\(getAppPath())/Settings.json"
    }

    private class func getAppPath() -> String {
        return NSHomeDirectory() + "/Library/CodeGen"
    }

    class func load() -> FilesystemSettingsGateway {
        let settingFilePath = getSettingFilePath()
        if FileManager.default.fileExists(atPath: settingFilePath) {
            let data = try! Data(contentsOf: URL(fileURLWithPath: settingFilePath))
            return try! JSONDecoder().decode(FilesystemSettingsGateway.self, from: data)
        } else {
            return FilesystemSettingsGateway()
        }
    }
}
