//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

class FileTemplateGenerator: TemplateGenerator {

    private let templateIO: TemplateIO
    private let resolver: TemplateResolver

    private var placeholders: [Placeholder]? = nil
    private var templates: [Template]? = nil

    init(_ templateIO: TemplateIO, _ resolver: TemplateResolver) {
        self.templateIO = templateIO
        self.resolver = resolver
    }

    func getPlaceholdersKeys(from templatesDir: File, mask: String = "~!(.+?)!~") throws -> [String] {
        templates = templateIO.findTemplates(at: templatesDir)
        return resolver.getPlaceholdersKeys(templates!, mask: mask)
    }

    func saveAt(dir: File, placeholders: [Placeholder]) {
        let resolvedTemplates = resolver.resolvePlaceholders(templates!, placeholders)
        templateIO.saveTemplates(at: dir, resolvedTemplates)
    }

    class func getDefault() -> FileTemplateGenerator {
        return FileTemplateGenerator(FileTemplateIO(), TemplateResolverImp())
    }
}
