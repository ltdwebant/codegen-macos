//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

class FileTemplateIO: TemplateIO {

    func findTemplates(at: File) -> [Template] {
        return recursiveSearch(at, at)
    }

    private func recursiveSearch(_ root: File, _ file: File) -> [Template] {
        if file.isDir() {
            return file.files().flatMap({ recursiveSearch(root, $0) })
        } else {
            if let text = file.toString() {
                let template = Template(text: text,
                        title: file.name(),
                        subPath: "subPath")
                return [template]
            } else {
                return []
            }
        }
    }

    func saveTemplates(at: File, _ templates: [ResolvedTemplate]) {
        templates.forEach { template in
            let url = URL(fileURLWithPath: at.getPath() + "/" + template.title)
            try! template.text.write(to: url,
                    atomically: true,
                    encoding: .utf8)
        }
    }
}

