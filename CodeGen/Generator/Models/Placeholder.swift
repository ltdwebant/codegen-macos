//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

struct Placeholder {

    let key: String
    var value: String
}
