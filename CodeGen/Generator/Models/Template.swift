//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

class Template {

    var text: String
    var title: String
    var subPath: String
    private var mask: String!

    init(text: String, title: String, subPath: String) {
        self.text = text
        self.title = title
        self.subPath = subPath
    }

    func getPlaceholders(mask: String) -> [String] {
        self.mask = mask
        var items: [String] = mask.r!.findAll(in: text).map { (element: Match) -> String in
            element.group(at: 1)!
        }
        if let item = mask.r!.findFirst(in: title)?.group(at: 1) {
            items.append(item)
        }
        return items
    }

    func resolve(_ resolver: VarResolver) -> ResolvedTemplate {
        let text = mask.r!.replaceAll(in: self.text) { match in
            return resolver.resolvePlaceholder(key: match.group(at: 1)!)
        }
        let title = mask.r!.replaceAll(in: self.title) { match in
            return resolver.resolvePlaceholder(key: match.group(at: 1)!)
        }
        return ResolvedTemplate(text: text, title: title, subPath: subPath)
    }
}
