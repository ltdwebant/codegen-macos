//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

class TemplateResolverImp: TemplateResolver, VarResolver {

    private var placeholders: [Placeholder]? = nil

    func getPlaceholdersKeys(_ templates: [Template], mask: String) -> [String] {
        let set = NSMutableSet()
        templates.flatMap({ $0.getPlaceholders(mask: mask) }).forEach { template in
            set.add(template)
        }
        return set.map({ $0 as! String })
    }

    func resolvePlaceholders(_ templates: [Template], _ placeholders: [Placeholder]) -> [ResolvedTemplate] {
        self.placeholders = placeholders
        return templates.map({ $0.resolve(self) })
    }

    func resolvePlaceholder(key: String) -> String {
        return placeholders!.first { placeholder in
            placeholder.key == key
        }!.value
    }
}
