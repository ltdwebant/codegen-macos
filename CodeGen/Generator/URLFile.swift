//
// Created by admin on 10.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

extension URL: File {

    func isDir() -> Bool {
        let values = try? self.resourceValues(forKeys: [.isDirectoryKey])
        return values?.isDirectory ?? false
    }

    func files() -> [File] {
        let files = try! FileManager.default.contentsOfDirectory(atPath: self.path)
                .filter({ String(Array($0)[min($0.count - 1, max(0, 0))]) != "." })
                .map({ URL(fileURLWithPath: self.path + "/" + $0) })
        return files
    }

    func toString() -> String? {
        return try? String(contentsOf: self)
    }

    func name() -> String {
        return self.lastPathComponent
    }

    func getPath() -> String {
        return self.path
    }
}
