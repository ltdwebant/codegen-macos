//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

protocol File {
    func isDir() -> Bool
    func files() -> [File]
    func toString() -> String?
    func name() -> String
    func getPath() -> String
}
