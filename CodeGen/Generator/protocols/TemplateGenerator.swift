//
//  TemplateGenerator.swift
//  CodeGen
//
//  Created by admin on 09.04.18.
//  Copyright © 2018 starmel. All rights reserved.
//

import Foundation

protocol TemplateGenerator {

    func getPlaceholdersKeys(from templatesDir: File, mask: String) throws -> [String]

    func saveAt(dir: File, placeholders: [Placeholder])
}