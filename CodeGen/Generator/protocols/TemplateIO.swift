//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

protocol TemplateIO {

    func findTemplates(at: File) -> [Template]

    func saveTemplates(at: File, _ templates: [ResolvedTemplate])
}
