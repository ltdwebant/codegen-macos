//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

protocol TemplateResolver {

    func getPlaceholdersKeys(_ templates: [Template], mask: String) -> [String]

    func resolvePlaceholders(_ templates: [Template], _ placeholders: [Placeholder]) -> [ResolvedTemplate]
}
