//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation

protocol VarResolver {

    func resolvePlaceholder(key: String) -> String
}
