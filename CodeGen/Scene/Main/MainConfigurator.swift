//
//  MainConfigurator.swift
//  CodeGen
//
//  Created by admin on 09.04.18.
//  Copyright © 2018 starmel. All rights reserved.
//

import Foundation

protocol MainConfigurator {
    func configure(view: ViewController)
}

class MainConfiguratorImp: MainConfigurator {

    func configure(view: ViewController) {

        let settingsGateway = FilesystemSettingsGateway.load()

        let router = MainRouterImp(view)
        let presenter = MainPresenterImp(view, router, settingsGateway)
        view.presenter = presenter
    }
}
