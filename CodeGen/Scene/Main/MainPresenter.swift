//
//  MainPresenter.swift
//  CodeGen
//
//  Created by admin on 09.04.18.
//  Copyright © 2018 starmel. All rights reserved.
//

import Foundation

protocol MainPresenter {

    func onSelectDestinationClick()
    func viewDidLoad()
    func onAddTemplateClick()
    func onGenerateClick(_ items: [Placeholder])
    func onTemplateSelectClick(_ row: Int)
    func onTemplateRemoveClick(at index: Int)
    func onTemplateDoubleClick(_ index: Int)
    func setPlaceholderMask(_ mask: String)
}

protocol MainView {

    func setDestinationTitle(_ title: String)
    func setPlaceholderMask(_ placeholder: String)
    func setAvailableTemplates(_ templates: [TemplateEntity])
    func setActivePlaceholders(_ keys: [String])
    func clearPlaceholders()
    func toggleGenerateButton(isActive: Bool)
}

class MainPresenterImp: MainPresenter {

    private let view: MainView
    private let router: MainRouter
    private var settings: SettingsGateway
    private let generator = FileTemplateGenerator.getDefault()

    private var destinationDir: URL? = nil
    private var activeTemplate: TemplateEntity? = nil
    private var activePlaceholderIndex = -1

    init(_ view: MainView, _ router: MainRouter, _ settings: SettingsGateway) {
        self.view = view
        self.router = router
        self.settings = settings
    }

    func onSelectDestinationClick() {
        router.openDirectoryPicker { url in
            self.destinationDir = url
            self.view.setDestinationTitle(url.lastPathComponent)
            self.updateGenerateButtonStatus()
        }
    }

    func viewDidLoad() {
        validateTemplates()
        view.setAvailableTemplates(settings.templates)
        view.setPlaceholderMask(settings.placeholder)
    }

    func onAddTemplateClick() {
        router.openFileOrDirectoryPicker { url in
            if url.toString() != nil || url.isDir() {
                self.settings.addTemplate(TemplateEntity(name: url.lastPathComponent, url: url))
                self.view.setAvailableTemplates(self.settings.templates)
                self.setActiveTemplate(self.settings.templates.count - 1)
            } else {
                self.router.showInvalidFileAlert()
            }
        }
    }

    func onTemplateSelectClick(_ row: Int) {
        setActiveTemplate(row)
    }

    func onGenerateClick(_ items: [Placeholder]) {
        generator.saveAt(dir: self.destinationDir!, placeholders: items)
        router.openFinder(at: self.destinationDir!)
    }

    func onTemplateRemoveClick(at index: Int) {
        if index > -1 {
            settings.removeTemplate(index)
            view.setPlaceholderMask(settings.placeholder)
            view.setAvailableTemplates(settings.templates)
            setActiveTemplate(index - 1)
        }
    }

    func onTemplateDoubleClick(_ index: Int) {
        if index > -1, let url = activeTemplate?.url {
            router.openFinder(at: url)
        }
    }

    func setPlaceholderMask(_ mask: String) {
        if let expression = try? NSRegularExpression(pattern: mask), expression.numberOfCaptureGroups == 1 {
            settings.placeholder = mask
            if activePlaceholderIndex > -1 {
                setActiveTemplate(activePlaceholderIndex)
            }
        } else {
            view.setPlaceholderMask(settings.placeholder)
            router.showInvalidPlaceholderMask()
        }
    }

    /*
     * Удалит из шаблонов те, у которых неверная ссылка на путь к файлу (файл был удален или перемещен).
     */
    private func validateTemplates() {
        let templates = settings.templates
        guard templates.count > 0 else {
            return
        }
        var nonExistTemplatesIndexes = [Int]()
        for i in 0..<templates.count {
            let template = templates[i]
            if !FileManager.default.fileExists(atPath: template.url.path) {
                nonExistTemplatesIndexes.append(i)
            }
        }
        if !nonExistTemplatesIndexes.isEmpty {
            for i in stride(from: nonExistTemplatesIndexes.count - 1, through: 0, by: -1) {
                let index = nonExistTemplatesIndexes[i]
                settings.removeTemplate(index)
            }
        }
    }

    private func updateGenerateButtonStatus() {
        view.toggleGenerateButton(isActive: destinationDir != nil && activeTemplate != nil)
    }

    private func setActiveTemplate(_ index: Int) {
        activePlaceholderIndex = index
        if index > -1 {
            activeTemplate = settings.templates[index]
            let keys = try! generator.getPlaceholdersKeys(from: activeTemplate!.url, mask: settings.placeholder)
            view.setActivePlaceholders(keys)
        } else {
            activeTemplate = nil
            view.clearPlaceholders()
        }
        updateGenerateButtonStatus()
    }
}
