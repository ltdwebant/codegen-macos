//
// Created by admin on 09.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Foundation
import Cocoa

protocol MainRouter {
    func openDirectoryPicker(_ onSelect: @escaping (URL) -> ())
    func openFileOrDirectoryPicker(_ onSelect: @escaping (URL) -> ())
    func openFinder(at: URL)
    func showInvalidPlaceholderMask()
    func showInvalidFileAlert()
}

class MainRouterImp: MainRouter {

    private let view: ViewController

    init(_ view: ViewController) {
        self.view = view
    }

    func openDirectoryPicker(_ onSelect: @escaping (URL) -> ()) {
        openSelector(canOpenFiles: false, onSelect: onSelect)
    }

    func openFileOrDirectoryPicker(_ onSelect: @escaping (URL) -> ()) {
        openSelector(canOpenFiles: true, onSelect: onSelect)
    }

    func openFinder(at: URL) {
        NSWorkspace.shared.open(at)
    }

    func showInvalidPlaceholderMask() {
        let alert = NSAlert()
        alert.messageText = "Placeholder must contain 1 regex capture group."
        alert.addButton(withTitle: "Close")
        alert.runModal()
    }

    func showInvalidFileAlert() {
        let alert = NSAlert()
        alert.messageText = "Invalid file encoding."
        alert.addButton(withTitle: "Close")
        alert.runModal()
    }

    private func openSelector(canOpenFiles: Bool, onSelect: (URL) -> ()) {
        let dialog = NSOpenPanel();
        dialog.title = "Select template or directory"
        dialog.showsResizeIndicator = true
        dialog.showsHiddenFiles = false
        dialog.canChooseDirectories = true
        dialog.canCreateDirectories = true
        dialog.canChooseFiles = canOpenFiles
        dialog.allowsMultipleSelection = false

        if (dialog.runModal() == .OK) {
            if let result = dialog.url {
                onSelect(result)
            }
        }
    }
}
