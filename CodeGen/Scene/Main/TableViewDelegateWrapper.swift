//
// Created by admin on 10.04.18.
// Copyright (c) 2018 starmel. All rights reserved.
//

import Cocoa

class TableViewDelegateWrapper<T>: NSObject, NSTableViewDelegate, NSTableViewDataSource {

    private let tableView: NSTableView
    private let createCell: (NSTableView, NSTableColumn?, T) -> NSView?
    private let onCellClick: (Int, Int) -> Void
    private let emptyMessage: String?
    private var emptyMessageView: NSView? = nil
    var items = [T]() {
        didSet {
            tableView.reloadData()
        }
    }

    init(_ tableView: NSTableView,
         _ createCell: @escaping (NSTableView, NSTableColumn?, T) -> NSView?,
         _ onCellClick: @escaping (Int, Int) -> Void,
         _ emptyMessage: String? = nil) {
        self.tableView = tableView
        self.createCell = createCell
        self.onCellClick = onCellClick
        self.emptyMessage = emptyMessage
        super.init()
        tableView.delegate = self
        tableView.dataSource = self
    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        return createCell(tableView, tableColumn, items[row])
    }

    func tableViewSelectionDidChange(_ notification: Notification) {
        onCellClick(tableView.selectedRow, tableView.selectedColumn)
    }

    func numberOfRows(in tableView: NSTableView) -> Int {
        if items.count == 0 && emptyMessageView == nil, let message = emptyMessage {
            let frame = tableView.frame
            let label = NSTextField(string: message)
            label.isEditable = false
            label.drawsBackground = false
            label.isBordered = false
            label.isSelectable = false
            label.frame = NSRect(
                    origin: CGPoint(x: frame.minX , y: frame.minY),
                    size: CGSize(width: frame.width, height: frame.height))
            label.alignment = .center
            tableView.addSubview(label)
            emptyMessageView = label
        } else if items.count > 0 && emptyMessageView != nil {
            emptyMessageView?.removeFromSuperview()
            emptyMessageView = nil
        }
        return items.count
    }

    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return CGFloat(25)
    }
}
