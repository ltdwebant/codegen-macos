//
//  ViewController.swift
//  CodeGen
//
//  Created by admin on 09.04.18.
//  Copyright © 2018 starmel. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, NSTextFieldDelegate {

    var presenter: MainPresenter!
    @IBOutlet weak var generateButton: NSButton!
    @IBOutlet weak var placeholdersTableView: NSTableView!
    @IBOutlet weak var destinationPickerButton: NSButton!
    @IBOutlet weak var destinationDirLabel: NSTextField!
    @IBOutlet weak var placeHolderMaskEditText: NSTextField!
    @IBOutlet weak var templatesTableView: NSTableView!
    lazy var templatesTableViewWrapper = {
        TableViewDelegateWrapper<TemplateEntity>(templatesTableView, { table, column, item in
            let label = NSTextField(string: item.name)
            label.isEditable = false
            label.drawsBackground = false
            label.isBordered = false
            label.isSelectable = false
            return label
        }, { row, column in
            self.presenter.onTemplateSelectClick(row)
        })
    }()
    lazy var placeholdersTableViewWrapper = {
        TableViewDelegateWrapper<Placeholder>(placeholdersTableView, { table, column, item in
            let label = NSTextField()
            label.drawsBackground = false
            label.isBordered = false
            if column?.identifier.rawValue == "PlaceholdersColumn" {
                label.isEditable = false
                label.isSelectable = false
                label.stringValue = item.key
            } else {
                label.stringValue = "Enter var"
            }
            return label
        }, { row, column in

        }, "Placeholders not found.")
    }()

    @IBAction func onDestinationButtonClick(_ sender: Any) {
        presenter.onSelectDestinationClick()
    }

    @IBAction func onPlaceholderMaskUpdate(_ sender: Any) {
        presenter.setPlaceholderMask(placeHolderMaskEditText.stringValue)
    }

    @IBAction func onGenerateButtonClick(_ sender: Any) {
        var items: [Placeholder] = placeholdersTableViewWrapper.items
        for row in 0..<placeholdersTableView.numberOfRows {
            let textField = placeholdersTableView.view(atColumn: 1, row: row, makeIfNecessary: true) as! NSTextField
            items[row].value = textField.stringValue
        }
        presenter.onGenerateClick(items)
    }

    @IBAction func onAddTemplateButtonClick(_ sender: Any) {
        presenter.onAddTemplateClick()
    }

    @IBAction func onRemoveTemplateButtonClick(_ sender: Any) {
        presenter.onTemplateRemoveClick(at: templatesTableView.selectedRow)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        MainConfiguratorImp().configure(view: self)

        templatesTableView.doubleAction = #selector(onTemplateDoubleClick)

        presenter.viewDidLoad()
    }

    @objc func onTemplateDoubleClick() {
        presenter.onTemplateDoubleClick(templatesTableView.selectedRow)
    }
}

extension ViewController: MainView {

    func setDestinationTitle(_ title: String) {
        destinationDirLabel.stringValue = "/" + title
    }

    func setPlaceholderMask(_ placeholder: String) {
        placeHolderMaskEditText.stringValue = placeholder
    }

    func setAvailableTemplates(_ templates: [TemplateEntity]) {
        templatesTableViewWrapper.items = templates
    }

    func setActivePlaceholders(_ keys: [String]) {
        placeholdersTableViewWrapper.items = keys.map({ Placeholder(key: $0, value: "") })
    }

    func clearPlaceholders() {
        placeholdersTableViewWrapper.items.removeAll()
    }

    func toggleGenerateButton(isActive: Bool) {
        generateButton.isEnabled = isActive
    }
}
